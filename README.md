﻿# Segunda Práctica

## Resolución con monitores

Para la solución de la práctica se utilizará como herramienta de concurrencia el desarrollo de monitores. Hay que tener presenta a la hora de implementar la solución que Java no dispone de esta herramienta. Para ello el alumno podrá utilizar cualquier utilidad de concurrencia para diseñar la clase que representa al monitor de la solución teórica. En la implementación del monitor se deberán respetar las características que presenta el monitor que se ha presentado en teoría. También se utilizará la factoría [`Executors`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Executors.html) y la interface [`ExecutorService`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas concurrentes que compondrán la solución de la práctica.

## Problema a resolver
Hay que diseñar un monitor que lleva el control del sistema de trasporte de una línea de autobús urbano. Para el diseño del monitor tenemos en cuenta las siguientes variables:

 - El número de paradas de la línea estará definida por un número **P** de paradas.
 - La cantidad de autobuses que componen la línea, es decir, hay un número de **N** autobuses que estarán recorriendo la línea.
 - Las paradas de la línea representan un recorrido secuencial que deberá realizar el autobús y que la siguiente parada de la última es otra vez la primera de la línea, es decir, es un recorrido circular.

El monitor deberá controlar las siguientes acciones:

 - Que un usuario espere en una parada la llegada de un autobús para dirigirse a su parada de destino.
 - Que el usuario deberá montar en el autobús cuando llegue a la parada donde se encuentra esperando si hay sitio en el autobús.
 - Que el usuario deje el autobús cuando llegue a la parada de destino de su viaje.
 - Que un autobús llegue a una parada para dejar a los usuarios que se bajan en esa parada antes de dejar subir a los que estén esperando en esa parada.
 - La nueva parada que en la que deberá parar el autobús. Si el autobús ya está lleno parará en la primera parada donde se bajen usuarios dado que no podrá recoger más gente hasta entonces.

Los procesos que formarán parte de la solución son los siguientes:

 - **Proceso usuario**, simulará las operaciones de un usuario con la línea de trasporte. Tendrá una parada de origen y una de destino que simulará un viaje para ese usuario.
 - **Proceso autobús**, simulará el recorrido que debe hacer un autobús en la línea de trasporte. Desde que se inicia su ejecución estará recorriendo la línea de transporte hasta que finalice la jornada. Cuando finaliza la jornada el autobús dejará a todos los usuarios que aún quedan dentro del autobús antes de finalizar y volver a cocheras.

En la resolución del ejercicio hay que tener en cuenta las siguientes restricciones:

 - Siempre que haya usuarios que quieran bajar en una parada serán los primero en hacerlo antes de que nuevos usuarios puedan subir al autobús.
 - No se puede superar la capacidad máxima del autobús en el transporte de usuarios.

